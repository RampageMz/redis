package com.redis.demo.controller;

import com.redis.demo.entity.LogTest;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Author:     Ma Zhen
 * Date:       2019/01/10 17:24
 * <p>
 * Content:    TestController:
 **/
@RestController
public class TestLogController {

    private static Logger logger = Logger.getLogger(TestLogController.class);

    @RequestMapping("/log")
    public String testLog() {
        logger.info("log: info.");
        logger.error("log: error!");

        return "hello";
    }

    @RequestMapping("/logTest")
    public String logEntity(){
        LogTest log=new LogTest(11,"Test entity");
        logger.error("info: "+log);
        logger.debug("info: "+log);
        logger.info("info: "+log);

        return "logTest";
    }

}
