package com.redis.demo.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Author:     Ma Zhen
 * Date:       2019/01/10 21:26
 * <p>
 * Content:    TestRedisController:
 **/

@RestController
public class TestRedisController {

    private static Logger logger = Logger.getLogger(TestRedisController.class);

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RequestMapping("/setRedis/{db}")
    public String setRedis(@PathVariable("db") Integer database) {
        JedisConnectionFactory jedisConnectionFactory = (JedisConnectionFactory) redisTemplate.getConnectionFactory();
        jedisConnectionFactory.setDatabase(database);
        redisTemplate.setConnectionFactory(jedisConnectionFactory);
        ValueOperations valueOperations = redisTemplate.opsForValue();
        valueOperations.set("test-db" + database, "key-" + database);

        return "redis:set";
    }

    @RequestMapping("/getRedis/{db}")
    public String getRedis(@PathVariable("db") Integer database) {
        JedisConnectionFactory jedisConnectionFactory = (JedisConnectionFactory) redisTemplate.getConnectionFactory();
        jedisConnectionFactory.setDatabase(database);
        redisTemplate.setConnectionFactory(jedisConnectionFactory);
        ValueOperations valueOperations = redisTemplate.opsForValue();

        return "redis:" + valueOperations.get("test-db" + database);
    }

}
