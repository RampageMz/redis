package com.redis.demo.entity;

/**
 * Author:     Ma Zhen
 * Date:       2019/01/10 17:37
 * <p>
 * Content:    LogTest:
 **/

public class LogTest {
    int no;
    String message;

    public LogTest(int no, String message) {
        this.no = no;
        this.message = message;
    }

    @Override
    public String toString() {
        return "LogTest{" +
                "no=" + no +
                ", message='" + message + '\'' +
                '}';
    }
}
